# Bot Discord Loup-Garou

## Installation

Use npm.

```bash
npm i
```

Start the application

```bash
npm run start
```

The environment variable `BOT_TOKEN` must be set to a valid discord bot token.

## Installation with Docker

Just run the latest version of the Docker image  
To change the prefix, you will need to modify the `config.json` file

```bash
docker run -d -e BOT_TOKEN=<your_bot_token> jeanlh/discordlgbot
```

## Configuration

The app contains a **config.json** file  
It should have a `prefix` property which will be the prefix used to call the bot commands  
By default the prefix is set to **.lg**

Example of **config.json**

```json
{
  "prefix": ".gl"
}
```
