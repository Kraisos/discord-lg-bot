const fs = require("fs");
const { MessageEmbed, MessageCollector, Collection } = require("discord.js");
const { readRoles } = require("../utils/rolesData");

module.exports = {
  name: "new",
  description: "Start a new game",
  aliases: ["n"],
  args: false,
  usage: `TODO`,
  cooldown: 1,
  async execute(message) {
    // Delete the message with the command
    message.delete();

    lgCategoryName = "Loup-Garou";
    infoChannelName = "loup-garou";

    // Create a new category (if it does not exist)
    let lgCategory = message.guild.channels.cache.find(
      (channel) => channel.name.includes(lgCategoryName) && !channel.deleted
    );

    if (lgCategory == null) {
      lgCategory = await message.guild.channels.create(lgCategoryName, {
        type: "category",
      });
    }

    // Create a new text channel (if it does not exist)
    let infoChannel = message.guild.channels.cache.find(
      (channel) => channel.name.includes(infoChannelName) && !channel.deleted
      // channel.parentID == lgCategory.id
    );
    if (infoChannel == null) {
      infoChannel = await message.guild.channels.create(infoChannelName, {
        type: "text",
        parent: lgCategory,
      });
    }

    // message.guild.channels.cache.forEach((c) => {
    //   console.log(
    //     `Channel Name: ${c.name} - channelparent: ${c.parent} -> ${
    //       c.parent == lgCategory
    //     }`
    //   );
    //   // if (c.name.includes(infoChannelName)) {
    //   //   console.log(c);
    //   // }
    // });

    // Send the embed visible by everyone
    // They can React to join the game
    // The MJ can lock the game

    let participationEmoji = "✋";
    let lockGameEmoji = "🔒";
    const infoEmbed = new MessageEmbed()
      .setTitle("Une nouvelle partie de loup Garou se prépare...")
      .setAuthor(`MJ : ${message.author.username}`, message.author.avatarURL())
      .setThumbnail(
        "https://www.jeuxetcompagnie.fr/wp-content/uploads/2011/11/loup-garou.jpg"
      )
      .setColor(0x00aa00)
      .setDescription("Réagi à ce message avec :raised_hand: pour participer")
      .addField("\u200B", "\u200B")
      .addField("Participants :", "\u200B");
    infoEmbedMessage = await infoChannel.send(infoEmbed);
    await infoEmbedMessage.react(participationEmoji);
    await infoEmbedMessage.react(lockGameEmoji);

    // The array that will store the participating users
    let participants = [];

    // Create the collector which will collect all reactions
    const filter = (reaction, user) => {
      return (
        reaction.emoji.name == participationEmoji ||
        reaction.emoji.name == lockGameEmoji
      );
    };
    const collector = infoEmbedMessage.createReactionCollector(filter, {
      dispose: true,
    });

    // Add the user to the participants array when he react
    //  -> update the embed
    // Lock the game when the MJ react
    collector.on("collect", (reaction, user) => {
      if (reaction.emoji.name == participationEmoji) {
        participants.push(user);
        updateEmbedParticipants(infoEmbedMessage, participants);
        infoEmbed.setColor(0xff0000);
        // console.log(`Collected ${reaction.emoji.name} from ${user.tag}`);
      } else if (
        reaction.emoji.name == lockGameEmoji &&
        user.id == message.author.id
      ) {
        collector.stop();
      }
    });

    // Remove the user from the participants array when he removes his reaction
    //   -> Update the embed
    collector.on("remove", (reaction, user) => {
      participants = participants.filter((u) => u.id != user.id);
      updateEmbedParticipants(infoEmbedMessage, participants);
      // console.log(
      //   `${user.username} removed his ${reaction.emoji.name} reaction !`
      // );
    });

    // When the collector ends
    //  -> change the embed to inform everyone
    //  -> Start the next step (MJ role configuration)
    collector.on("end", async (collected) => {
      newEmbed = new MessageEmbed(infoEmbedMessage.embeds[0]);
      newEmbed.setTitle(
        "La partie est en préparation - impossible de rejoindre"
      );
      newEmbed.setColor(0xff0000);
      newEmbed.setDescription(
        "Attendez que le maître du jeu configure les rôles"
      );
      infoEmbedMessage = await infoEmbedMessage.edit(newEmbed);

      infoEmbedMessage.reactions.removeAll();

      roleConfiguration(message, lgCategory, participants, infoEmbedMessage);
    });

    // author = message.member;

    // author.voice.setDeaf();
  },
};

async function roleConfiguration(
  message,
  lgCategory,
  participants,
  infoEmbedMessage
) {
  // mjConsole = message.channel;
  // console.log(message.guild);

  // Create a new mj channel (if it does not exist)
  let mjChannelName = "console-mj";
  let mjChannel = message.guild.channels.cache.find(
    (channel) => channel.name.includes(mjChannelName) && !channel.deleted
    // channel.parent == lgCategory
  );

  let permissionOverwrites = [
    {
      id: message.guild.id,
      deny: ["VIEW_CHANNEL"],
    },
    {
      id: message.author.id,
      allow: ["VIEW_CHANNEL"],
    },
  ];
  if (mjChannel == null) {
    mjChannel = await message.guild.channels.create(mjChannelName, {
      type: "text",
      parent: lgCategory,
      permissionOverwrites,
    });
  } else {
    mjChannel.overwritePermissions(permissionOverwrites);
  }

  let roles = readRoles();
  let addedRoles = [];
  let addedRolesQuantity = 0;

  let okSent = false;
  let okCollectorCreated = false;

  let okEmoji = "✅";

  rolesString = "";
  roles.forEach((r) => {
    rolesString += r.emoji + " " + "`" + r.name + "` \n";
  });
  if (roles.length == 0) {
    addedRolesString =
      "Aucun rôle défini pour le moment\n" +
      "utilise la commande .lg roles pour ajouter des rôles";
  }

  addedRolesString = "";
  addedRoles.forEach((r) => {
    addedRolesString +=
      r.quantity + " " + r.emoji + " " + "`" + r.name + "` \n";
  });
  if (addedRoles.length == 0) {
    addedRolesString = "Aucun rôle ajouté pour le moment";
  }

  const roleConfigEmbed = new MessageEmbed()
    .setTitle("Configuration des rôles")
    .setAuthor(`MJ : ${message.author.username}`, message.author.avatarURL())
    .setColor(0xff0000)
    .setDescription(
      `Nombre de joueurs : \`${participants.length}\`\n` +
        "Sélectionne un rôle pour l'ajouter à la partie\n" +
        "Tu peux le sélectionner de nouveau pour changer le nombre"
    )
    .addField("\u200B", "\u200B")
    .addField(
      "Rôles ajoutés [" + addedRolesQuantity + "/" + participants.length + "]",
      addedRolesString
    )
    .addField("\u200B", "\u200B")
    .addField("Rôles disponibles", rolesString);

  roleConfigEmbedMessage = await mjChannel.send(roleConfigEmbed);

  roles.forEach(async (r) => {
    await roleConfigEmbedMessage.react(r.emoji);
  });

  const filter = (reaction, user) => {
    var found = false;
    for (let i = 0; i < roles.length; i++) {
      if (roles[i].emoji == reaction.emoji.name) {
        found = true;
        break;
      }
    }
    return found && user.id == message.author.id;
  };
  const roleReactionCollector = roleConfigEmbedMessage.createReactionCollector(
    filter
  );

  roleReactionCollector.on("collect", async (reaction, user) => {
    let modification = false;

    // remove the user reaction
    reaction.users.remove(user.id);

    if (addedRoles.map((r) => r.emoji).includes(reaction.emoji.name)) {
      addedRole = addedRoles.find((r) => r.emoji == reaction.emoji.name);
      modification = true;
    } else {
      addedRole = {
        ...roles.find((r) => r.emoji == reaction.emoji.name),
      };
    }

    newEmbed = new MessageEmbed(roleConfigEmbedMessage.embeds[0]);
    newEmbed.spliceFields(0, 0, [
      { name: "\u200B", value: "\u200B" },

      {
        name: `${modification ? "Modification" : "Ajout"} du rôle: ${
          addedRole.emoji
        } \`${addedRole.name}\``,
        value:
          "Combien en veux-tu dans la partie ?\n" +
          "Envoie un nombre dans le chat",
      },
    ]);
    roleConfigEmbedMessage = await roleConfigEmbedMessage.edit(newEmbed);

    const messageCollector = new MessageCollector(
      mjChannel,
      (m) => m.author.id === message.author.id
    );

    messageCollector.on("collect", async (m) => {
      m.delete();
      messageCollector.stop();

      if (modification) {
        addedRolesQuantity -= addedRole.quantity;
      }

      // TODO check if it's an int
      addedRole.quantity = parseInt(m.content);

      if (!modification) {
        addedRoles.push(addedRole);
      }

      // Remove the roles qith quantity 0 or less
      addedRoles = addedRoles.filter((r) => r.quantity > 0);

      addedRolesQuantity += addedRole.quantity;

      addedRolesString = "";
      addedRoles.forEach((r) => {
        addedRolesString +=
          r.quantity + " " + r.emoji + " " + "`" + r.name + "` \n";
      });
      if (addedRoles.length == 0) {
        addedRolesString = "Aucun rôle ajouté pour le moment";
      }

      newEmbed = new MessageEmbed(roleConfigEmbedMessage.embeds[0]);
      newEmbed.spliceFields(1, 3, {
        name:
          "Rôles ajoutés [" +
          addedRolesQuantity +
          "/" +
          participants.length +
          "]",
        value: addedRolesString,
      });

      roleConfigEmbedMessage = await roleConfigEmbedMessage.edit(newEmbed);

      if (addedRolesQuantity == participants.length) {
        await roleConfigEmbedMessage.react(okEmoji);
        okSent = true;

        if (!okCollectorCreated) {
          const okCollector = roleConfigEmbedMessage.createReactionCollector(
            (reaction, user) => {
              return (
                reaction.emoji.name == okEmoji && user.id == message.author.id
              );
            }
          );

          okCollector.on("collect", () => {
            roleReactionCollector.stop();
            okCollector.stop();

            startGame(
              message,
              participants,
              addedRoles,
              roleConfigEmbedMessage,
              infoEmbedMessage,
              mjChannel
            );
          });

          okCollectorCreated = true;
        }
      } else if (okSent) {
        roleConfigEmbedMessage.reactions.cache.get(okEmoji).remove();
        okSent = false;
      }
    });
  });
}

async function startGame(
  message,
  participants,
  addedRoles,
  roleConfigEmbedMessage,
  infoEmbedMessage,
  mjChannel
) {
  // Wiil be a collection (map): User => Role
  // Role is: {name: <name>, emoji: <emoji>}
  // User is a discord user
  let village = new Collection();

  p = [...participants];
  addedRoles.forEach((r) => {
    for (let i = 0; i < r.quantity; i++) {
      let user = p.splice(Math.floor(Math.random() * p.length), 1)[0];
      let role = {
        name: r.name,
        emoji: r.emoji,
      };
      village.set(user, role);

      dmEmbed = new MessageEmbed()
        .setColor(0x0099e1)
        .setAuthor(
          `MJ : ${message.author.username}`,
          message.author.avatarURL()
        )
        .setTitle("Ton rôle")
        .setDescription(`${role.emoji} \xa0 ${role.name}`);

      user.send(dmEmbed);
    }
  });

  // console.log(village);

  let villageString = "";

  village.forEach((value, key) => {
    villageString += `\`${key.username}\` \xa0 ${value.emoji} \xa0 \`${value.name}\`\n`;
  });

  newInfoEmbed = new MessageEmbed(infoEmbedMessage.embeds[0])
    .setTitle("Partie en cours")
    .setColor(0x0099e1)
    .setDescription(
      "Suivez les instructions du MJ\n" + "Vous avez reçu vôtre rôle en MP"
    );

  infoEmbedMessage = infoEmbedMessage.edit(newInfoEmbed);

  roleConfigEmbedMessage.reactions.removeAll();
  newEmbed = new MessageEmbed(roleConfigEmbedMessage.embeds[0])
    .setTitle("Partie lancée")
    .setColor(0x0099e1)
    .setDescription("")
    .spliceFields(0, 4)
    //.addField("\u200B", "\u200B")
    .addField("Composition du village", villageString);
  roleConfigEmbedMessage = await roleConfigEmbedMessage.edit(newEmbed);
  roleConfigEmbedMessage.react("💯");
  addedRoles.forEach((r) => {
    roleConfigEmbedMessage.react(r.emoji);
  });

  muteEmbed = new MessageEmbed()
    .setTitle("Aucun rôle selectionné")
    .setDescription("Choisi l'action à effectuer")
    .setColor(0x0099e1);

  let muteEmbedMessage = await mjChannel.send(muteEmbed);

  const filter = (reaction, user) => {
    if (user.id != message.author.id) {
      return false;
    }

    if (reaction.emoji.name == "💯") {
      return true;
    }

    var found = false;
    for (let i = 0; i < addedRoles.length; i++) {
      if (addedRoles[i].emoji == reaction.emoji.name) {
        found = true;
        break;
      }
    }
    return found;
  };
  const roleReactionCollector = roleConfigEmbedMessage.createReactionCollector(
    filter
  );

  let selectedRole;
  let everyone;
  roleReactionCollector.on("collect", async (reaction, user) => {
    reaction.users.remove(user.id);

    everyone = reaction.emoji.name == "💯";

    if (!everyone) {
      selectedRole = {
        ...addedRoles.find((r) => r.emoji == reaction.emoji.name),
      };
    } else {
      selectedRole = {
        name: "Tout le village",
        emoji: "💯",
      };
    }

    newMuteEmbed = new MessageEmbed(muteEmbed).setTitle(
      `${selectedRole.emoji} \xa0 \`${selectedRole.name}\``
    );
    muteEmbedMessage = await muteEmbedMessage.edit(newMuteEmbed);
  });

  await muteEmbedMessage.react("<:unmute:779003735882989608>");
  await muteEmbedMessage.react("<:mute:779003749724979210>");
  await muteEmbedMessage.react("<:undeafen:779000730337542164>");
  await muteEmbedMessage.react("<:deafen:779002513851416616>");

  muteReactionCollector = muteEmbedMessage.createReactionCollector(
    (r, u) => u.id == message.author.id
  );

  // GuildMemberManager (we can't mute a User, but we can mute a GuildMember)
  let members = message.guild.members;
  muteReactionCollector.on("collect", async (reaction, user) => {
    // console.log("Collect " + reaction.emoji.name);

    reaction.users.remove(user.id);

    let guildMembers;
    if (!everyone) {
      guildMembers = village
        .filter((role) => role.name == selectedRole.name)
        .map((role, user) => members.cache.get(user.id));
    } else {
      guildMembers = village.map((role, user) => members.cache.get(user.id));
    }

    switch (reaction.emoji.name) {
      case "lgunmute":
        guildMembers.forEach(async (guildMember) => {
          // console.log("unmute " + guildMember.user.name);
          await guildMember.voice.setMute(false);
        });
        break;

      case "lgmute":
        guildMembers.forEach(async (guildMember) => {
          // console.log("mute " + guildMember.user.name);
          await guildMember.voice.setMute(true);
        });
        break;

      case "lgundeafen":
        guildMembers.forEach(async (guildMember) => {
          // console.log("undeafen " + guildMember.user.name);
          await guildMember.voice.setDeaf(false);
          // await guildMember.voice.setMute(false);
        });
        break;

      case "lgdeafen":
        guildMembers.forEach(async (guildMember) => {
          // console.log("deafen " + guildMember.user.name);
          await guildMember.voice.setDeaf(true);
          // await guildMember.voice.setMute(true);
        });
        break;
      default:
        break;
    }
  });
}

function updateEmbedParticipants(infoEmbedMessage, participants) {
  participantsString = "";

  participants.forEach((u) => {
    participantsString += "`" + u.username + "` ";
  });

  if (participants.length == 0) {
    participantsString = "\u200B";
  }

  // Remove the fields
  newEmbed = new MessageEmbed(infoEmbedMessage.embeds[0]);
  newEmbed.spliceFields(1, 2);
  newEmbed.addField("Participants", participantsString, true);
  infoEmbedMessage.edit(newEmbed);
}
