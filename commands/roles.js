const { MessageEmbed, MessageCollector } = require("discord.js");
const {
  readRoles,
  writeRole,
  removeRole,
  replaceRole,
} = require("../utils/rolesData");

module.exports = {
  name: "rôles",
  description: "Edit the game's rôles",
  aliases: ["r"],
  args: false,
  usage: `TODO`,
  cooldown: 10,
  async execute(message, args) {
    message.delete();

    let roles = readRoles();
    let addRoleEmoji = "➕";

    const rolesEmbed = createRolesEmbed(roles);
    rolesEmbedMessage = await message.channel.send(rolesEmbed);

    await rolesEmbedMessage.react(addRoleEmoji);
    roles.forEach(async (r) => {
      await rolesEmbedMessage.react(r.emoji);
    });

    // TODO: add the possibility to edit the roles by typing its name (faster than emojis)

    const filter = (reaction, user) => {
      if (user.id != message.author.id) {
        return false;
      }

      if (reaction.emoji.name == addRoleEmoji) {
        return true;
      }

      var found = false;
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].emoji == reaction.emoji.name) {
          found = true;
          break;
        }
      }

      return found;
    };

    const reactionCollector = rolesEmbedMessage.createReactionCollector(filter);

    reactionCollector.on("collect", (reaction, user) => {
      reaction.users.remove(user.id);
      switch (reaction.emoji.name) {
        case "➕":
          addRole(message, rolesEmbedMessage);
          break;

        default:
          role = roles.find((r) => r.emoji == reaction.emoji.name);
          if (role != null) {
            editRole(message, role, rolesEmbedMessage);
          }
          break;
      }
    });
  },
};

function createRolesEmbed(roles) {
  rolesString = "";
  roles.forEach((r) => {
    rolesString += r.emoji + " " + "`" + r.name + "` \n";
  });
  if (roles.length == 0) {
    rolesString = "Aucun rôle ajouté pour le moment";
  }

  const embed = new MessageEmbed()
    .setTitle("Rôles sauvegardés")
    .setColor(0xff0000)
    .setDescription(
      "Pour modifier un rôle, réagis avec l'émote correspondante\n" +
        "Pour Ajouter un rôle, réagi avec ➕"
    )
    .addField("\u200B", "\u200B")
    .addField("Rôles", rolesString);

  return embed;
}

async function addRole(message, rolesEmbedMessage) {
  let saveRoleEmoji = "✅";
  let cancelEmoji = "❎";

  console.log("addRole");
  const createRoleEmbed = new MessageEmbed()
    .setTitle("Ajout d'un nouveau rôle 1/2")
    .setColor(0x00cc00)
    .setDescription(
      "Quel est le nom du nouveau rôle ?\n" +
        "(écris le nom du rôle dans le chat)"
    );
  createRoleEmbedMessage = await message.channel.send(createRoleEmbed);

  newRole = {};

  const messageCollector = new MessageCollector(
    message.channel,
    (m) => m.author.id === message.author.id
  );

  messageCollector.on("collect", async (m) => {
    newRole.name = m.content;

    m.delete();

    newCreateRoleEmbed = new MessageEmbed(createRoleEmbedMessage.embeds[0])
      .setTitle("Ajout d'un nouveau rôle 2/2")
      .setDescription(
        "Quel est l'emoji du nouveau rôle ?\n" +
          "(réagi à ce message avec l'emoji désiré)\n"
      )
      .addField("Nom du rôle", newRole.name);
    createRoleEmbedMessage = await createRoleEmbedMessage.edit(
      newCreateRoleEmbed
    );

    messageCollector.stop();

    const emojiCollector = createRoleEmbedMessage.createReactionCollector(
      (r, u) => u.id == message.author.id
    );

    emojiCollector.on("collect", async (reaction, user) => {
      if (!newRole.emoji) {
        newRole.emoji = reaction.emoji.name;

        newCreateRoleEmbed = new MessageEmbed(createRoleEmbedMessage.embeds[0])
          .setTitle("Ajout d'un nouveau rôle - contrôle")
          .setDescription(
            "Vérifie les informations\n" +
              `${saveRoleEmoji}: Sauvegarder le rôle\n` +
              `${cancelEmoji}: Annuler la création du rôle`
          )
          .addField("Emoji du rôle", newRole.emoji);
        createRoleEmbedMessage = await createRoleEmbedMessage.edit(
          newCreateRoleEmbed
        );

        createRoleEmbedMessage.reactions.removeAll();
        await createRoleEmbedMessage.react(saveRoleEmoji);
        await createRoleEmbedMessage.react(cancelEmoji);

        emojiCollector.on("collect", (reaction, user) => {
          // TODO: update roles embed after creating a new role
          switch (reaction.emoji.name) {
            case saveRoleEmoji:
              writeRole(newRole);

            case cancelEmoji:
              createRoleEmbedMessage.delete();
              rolesEmbedMessage.delete();
              break;
            default:
              reaction.users.remove(user.id);
              break;
          }
        });
      }
    });
  });
}

async function editRole(message, role, rolesEmbedMessage) {
  let saveRoleEmoji = "✅";
  let cancelRoleEmoji = "❎";
  let deleteRoleEmoji = "❌";

  const editRoleEmbed = new MessageEmbed()
    .setTitle(`Modification du rôle ${role.emoji} \`${role.name}\``)
    .setColor(0xe6e600)
    .setDescription(
      "Envoie un message pour modifier le nom du rôle\n" +
        "Réagis à ce message pour changer l'emoji du rôle\n" +
        `${saveRoleEmoji}: Sauvegarder le rôle\n` +
        `${cancelRoleEmoji}: Annuler la modification\n` +
        `${deleteRoleEmoji}: Supprimmer le rôle`
    )
    .addField("Nouveau nom", role.name)
    .addField("Nouvel emoji", role.emoji);
  editRoleEmbedMessage = await message.channel.send(editRoleEmbed);
  await editRoleEmbedMessage.react(saveRoleEmoji);
  await editRoleEmbedMessage.react(cancelRoleEmoji);
  await editRoleEmbedMessage.react(deleteRoleEmoji);

  let editedRole = { ...role };

  const emojiCollector = editRoleEmbedMessage.createReactionCollector(
    (r, u) => u.id == message.author.id
  );

  const messageCollector = new MessageCollector(
    message.channel,
    (m) => m.author.id === message.author.id
  );

  emojiCollector.on("collect", async (reaction, user) => {
    reaction.users.remove(user.id);

    // TODO: update roles embed after editing/deleting a new role
    switch (reaction.emoji.name) {
      case saveRoleEmoji:
        replaceRole(role.name, editedRole);
        emojiCollector.stop();
        messageCollector.stop();
        editRoleEmbedMessage.delete();
        rolesEmbedMessage.delete();
        break;

      case cancelRoleEmoji:
        emojiCollector.stop();
        messageCollector.stop();
        editRoleEmbedMessage.delete();
        rolesEmbedMessage.delete();
        break;

      case deleteRoleEmoji:
        removeRole(role.name);
        emojiCollector.stop();
        messageCollector.stop();
        editRoleEmbedMessage.delete();
        rolesEmbedMessage.delete();
        break;

      default:
        editedRole.emoji = reaction.emoji.name;

        const newEditRoleEmbed = new MessageEmbed(
          editRoleEmbedMessage.embeds[0]
        ).spliceFields(1, 1, { name: "Nouvel emoji", value: editedRole.emoji });
        editRoleEmbedMessage = await editRoleEmbedMessage.edit(
          newEditRoleEmbed
        );
        break;
    }
  });

  messageCollector.on("collect", async (message) => {
    message.delete();

    editedRole.name = message.content;

    const newEditRoleEmbed = new MessageEmbed(
      editRoleEmbedMessage.embeds[0]
    ).spliceFields(0, 1, { name: "Nouveau nom", value: editedRole.name });
    editRoleEmbedMessage = await editRoleEmbedMessage.edit(newEditRoleEmbed);
  });
}
