const { Channel } = require("discord.js");

module.exports = {
  name: "stop",
  aliases: ["s"],
  description: "Arrête la game de lg",
  execute(message) {
    message.delete();

    message.guild.channels.cache.forEach((channel) => {
      if (
        channel.name.includes("Loup-Garou") ||
        channel.name.includes("console-mj") ||
        channel.name.includes("loup-garou")
      )
        // if the string '-old' is found within the channel name
        channel
          .delete() // delete the channel
          .then(() => console.log(`Deleted ${channel.name}`))
          .catch((e) =>
            console.log(`Could not delete ${channel.name} because of ${e}`)
          ); // handle any errors
    });
  },
};
