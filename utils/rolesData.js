const fs = require("fs");

// TODO: env. variable ?
const rolesPath = "./roles.json";

function sortRoles(roles) {
  return roles.sort((a, b) =>
    a.name.toLowerCase().localeCompare(b.name.toLowerCase())
  );
}

function readRoles() {
  return JSON.parse(fs.readFileSync(rolesPath));
}

function writeRole(role) {
  let roles = readRoles();
  roles.push(role);
  sortRoles(roles);

  const jsonString = JSON.stringify(roles, null, 2);

  fs.writeFile(rolesPath, jsonString, (err) => {
    if (err) {
      console.log("Error writing file", err);
    }
  });
}

// Remove a role by name
function removeRole(name) {
  let roles = readRoles();
  roles = roles.filter((r) => r.name != name);

  const jsonString = JSON.stringify(roles, null, 2);

  fs.writeFile(rolesPath, jsonString, (err) => {
    if (err) {
      console.log("Error writing file", err);
    }
  });
}

function replaceRole(roleName, newRole) {
  let roles = readRoles();
  roles = roles.filter((r) => r.name != roleName);
  roles.push(newRole);
  sortRoles(roles);

  const jsonString = JSON.stringify(roles, null, 2);

  fs.writeFile(rolesPath, jsonString, (err) => {
    if (err) {
      console.log("Error writing file", err);
    }
  });
}

module.exports = {
  sortRoles,
  readRoles,
  writeRole,
  removeRole,
  replaceRole,
};
